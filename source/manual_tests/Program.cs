﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

using masscarade.product_definition.feature;
using masscarade.generation;

namespace manual_tests
{
    class Program
    {
        static void Main(string[] args)
        {

            Feature feature = Feature.getSample();
            XmlWriterSettings settings = new XmlWriterSettings()
            {
                Indent = true,
            };

            using (XmlWriter write = XmlWriter.Create("feature_sample.xml", settings))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(Feature));

                serializer.WriteObject(write, feature);
            }

            Feature check;

            using (XmlReader reader = XmlReader.Create("feature_sample.xml"))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(Feature));

                check = (Feature)serializer.ReadObject(reader);
            }

            Project project = masscarade.BuR.project.Project.getSample();

            using (XmlWriter write = XmlWriter.Create("BuRproject_sample.xml", settings))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(Project));

                serializer.WriteObject(write, project);
            }

            Project checkProject;

            using (XmlReader reader = XmlReader.Create("BuRproject_sample.xml"))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(Project));

                checkProject = (Project)serializer.ReadObject(reader);
            }

            masscarade.Order order = masscarade.Order.getSample();

            using (XmlWriter write = XmlWriter.Create("order_sample.xml", settings))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(masscarade.Order));

                serializer.WriteObject(write, order);
            }

            masscarade.generation.Generator.Settings genSettings = masscarade.generation.Generator.Settings.getSample();

            using (XmlWriter write = XmlWriter.Create("generator_settings_sample.xml", settings))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(masscarade.generation.Generator.Settings));

                serializer.WriteObject(write, genSettings);
            }
        }
    }
}
