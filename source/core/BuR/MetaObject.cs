﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using masscarade.product_instance;

namespace masscarade.BuR
{
    public abstract class MetaObject
    {
        public abstract void generate(project.Project project);

        public ProductInstance instance { get; protected set; }

        public FileInfo sourceData { get; protected set; }

        public ConfigurationObject obj { get; protected set; }

        //public ProductInstance productInstance { get; protected set; }

        public class Factory
        {
            /// <summary>
            /// collection al all known methods of producing metaobjects
            /// </summary>
            public static Dictionary<string, ProduceDelegate> produceMethods = new Dictionary<string, ProduceDelegate>();
            public delegate MetaObject ProduceDelegate(ProductInstance inst, ConfigurationObject obj, FileInfo src);

            static Factory()
            {
                produceMethods.Add(logical.Task.EXTENSION, logical.Task.produce);
            }

            public MetaObjectContainer produced { get; private set; } = new MetaObjectContainer();

            public MetaObjectContainer produce(Order order, product_definition.ProductDefinitionDictionary products)
            {
                MetaObjectContainer container = new MetaObjectContainer();

                foreach (KeyValuePair<ProductInstance.ID, ProductInstance> pair in order.products)
                {
                    container.Add(produce(pair.Value, products));                   
                }

                return container;
            }

            public MetaObjectContainer produce(ProductInstance parent, product_definition.ProductDefinitionDictionary products)
            {
                MetaObjectContainer container = new MetaObjectContainer();

                ProductInstance inst = parent;
                inst.definition = products[inst.definition.id];

                List<MetaObject> objects = new List<MetaObject>();

                foreach (ConfigurationObject obj in inst.definition.feature.configurationObjects)
                {

                    foreach (FileInfo file in obj.getFiles())
                    {
                        MetaObject elemet = produce(inst, obj, file);

                        if (elemet != null)
                        {
                            objects.Add(elemet);
                        }
                    }
                }

                if (container.ContainsKey(inst) == false)
                {
                    container[inst] = new List<MetaObject>();
                }

                container[inst].AddRange(objects);

                foreach (product_definition.feature.rules.SlotElement slot in parent.slots)
                {
                    foreach (ProductInstance child in slot)
                    {
                        container.Add(produce(child, products));
                    }
                }

                return container;
            }

            public MetaObjectContainer produce(ProductInstance inst, ConfigurationObject obj)
            {
                throw new NotImplementedException();
            }

            public MetaObject produce(ProductInstance inst, ConfigurationObject obj, FileInfo file)
            {
                if (produceMethodAvailable(file) == true)
                {
                    return produceMethods[file.Extension](inst, obj, file);
                }

                return null;
            }

            /// <summary>
            /// chekcs wheter the provided file can be produced to a metaobject
            /// </summary>
            /// <param name="file"></param>
            /// <returns></returns>
            public bool produceMethodAvailable(FileInfo file)
            {
                return produceMethods.ContainsKey(file.Extension);
            }
        }             
    }

    public class MetaObjectContainer : Dictionary<ProductInstance, List<MetaObject>>
    {
        public void Add(ProductInstance inst, MetaObject obj)
        {
            if (this.ContainsKey(inst) == false)
            {
                this.Add(inst, new List<MetaObject>());
            }       

            this[inst].Add(obj);
        }

        public void Add(MetaObjectContainer container)
        {
            foreach (KeyValuePair<ProductInstance, List<MetaObject>> pair in container)
            {
                if (this.ContainsKey(pair.Key) == false)
                {
                    this.Add(pair.Key, new List<MetaObject>());
                }

                this[pair.Key].AddRange(pair.Value);
            }
        }
    }
}
