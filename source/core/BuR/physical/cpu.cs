﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;

namespace masscarade.BuR.physical
{

    public partial class SwConfiguration
    {
        private FileInfo location { get; set; }
        public class NoTaskPropertiesException : Exception
        {
        }

        public class MoreThanOneTaskPropertiesException : Exception
        {
        }

        public void save(string path)
        {
            //backup sw configuration
            //System.IO.File.Copy(path, path + "_old", true);

            using (TextWriter writer = new StreamWriter(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SwConfiguration));
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                serializer.Serialize(writer, this);

                location = new FileInfo(path);
            }
        }

        public void save()
        {
            save(location.FullName);
        }

        public static SwConfiguration load(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SwConfiguration));

                SwConfiguration swConfig = (SwConfiguration)serializer.Deserialize(fs);
                swConfig.location = new FileInfo(path);

                return swConfig;
            }
        }

        public void add(BuR.logical.Task inst)
        {
            ConfigurationObject.PropetiesList properties = inst.obj.properties[typeof(logical.TaskProperties)];

            if (properties.Count == 0)
            {
                throw new NoTaskPropertiesException();
            }
            else if (properties.Count > 1)
            {
                throw new MoreThanOneTaskPropertiesException();
            }

            logical.TaskProperties taskProperties = (logical.TaskProperties)properties[0];

            foreach (SwConfigurationTaskClass taskClass in TaskClass)
            {
                if (taskClass.Name == taskProperties.taskClass.text)
                {
                    SwConfigurationTaskClassTask task = new SwConfigurationTaskClassTask()
                    {
                        Debugging = taskProperties.debugging.ToString().ToLower(),
                        Language = taskProperties.language.text,
                        Memory = taskProperties.memory.text,
                        Description = "auto generated " + inst.instance.definition.id.ToString(),
                        Name = inst.instance.id.ToString(),
                        Source = (Regex.Match(inst.sourceData.FullName, @"(?<=Logical\\)(.*)(?=\\" + taskProperties.language.text + @".prg)").Value + ".prg")
                            .Replace(@"\", @"."),
                    };

                    if (taskClass.Task == null)
                    {
                        /*add first member to taskclass*/
                        taskClass.Task = new SwConfigurationTaskClassTask[1];
                        taskClass.Task[0] = task;
                    }
                    else
                    {
                        /*add task to taskclass*/
                        List<SwConfigurationTaskClassTask> tasks = new List<SwConfigurationTaskClassTask>(taskClass.Task);

                        if (tasks.Contains(task) == false)
                        {
                            tasks.Add(task);
                            taskClass.Task = tasks.ToArray();
                        }
                    }
                }
            }
        }
    }
}

