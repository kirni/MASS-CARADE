﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace masscarade.BuR
{
    [DataContract]
    [KnownType(typeof(ConfigFile))]
    [KnownType(typeof(ConfigFolder))]    
    public abstract class ConfigurationObject : product_definition.feature.ConfigurationObject
    {
        [DataMember]
        public project.ProjectSource sourceProject { get; protected set; }

        [DataMember]
        public string path { get; protected set; }

        [DataContract]
        [KnownType(typeof(logical.TaskProperties))]
        public abstract class BuRProperties : Properties
        {
        }

        public abstract List<FileInfo> getFiles();
    }

    [DataContract]
    public class ConfigFile : ConfigurationObject
    {

        private ConfigFile() { }

        public override bool Equals(product_definition.feature.ConfigurationObject other)
        {
            if (other.GetType() == this.GetType())
            {
                ConfigFile obj = (ConfigFile)other;

                return this.path.Equals(obj.path) && this.sourceProject.Equals(obj.sourceProject);
            }

            return false;
        }      

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 * path.GetHashCode();
            hash = hash * 23 * sourceProject.GetHashCode();

            return hash;
        }

        public static ConfigFile getSample()
        {
            return new ConfigFile()
            {
                sourceProject = project.ProjectSource.Local.getSample(),
                path = @"path/to/folder",
                properties = new PropetiesList(new Properties[] { logical.TaskProperties.getSample(), ConfigurationObject.ReplacePattern.getSample()}),
            };
        }

        public override List<FileInfo> getFiles()
        {
            List<FileInfo> files = new List<FileInfo>();
            files.Add(new FileInfo(path));

            return files;
        }
    }

    [DataContract]
    public class ConfigFolder : ConfigurationObject
    {
        [DataMember]
        public bool recursive { get; private set; }

        private ConfigFolder() { }

        public override bool Equals(product_definition.feature.ConfigurationObject other)
        {
            if (other.GetType() == this.GetType())
            {
                ConfigFolder obj = (ConfigFolder)other;

                return this.path.Equals(obj.path) && this.sourceProject.Equals(obj.sourceProject);
            }

            return false;
        }

        public override int GetHashCode()
        {
            int hash = 19;
            hash = hash * 27 * path.GetHashCode();
            hash = hash * 27 * sourceProject.GetHashCode();

            return hash;
        }

        public static ConfigFolder getSample()
        {
            return new ConfigFolder()
            {
                sourceProject = project.ProjectSource.Local.getSample(),
                path = @"path/to/file.ending",
                properties = new PropetiesList(new Properties[] {ConfigurationObject.ReplacePattern.getSample() }),
            };
        }

        public override List<FileInfo> getFiles()
        {
            List<FileInfo> files = new List<FileInfo>();
            
            foreach (string file in Directory.GetFiles(path))
            {
                files.Add(new FileInfo(file));
            }

            return files;
        }
    }
}
