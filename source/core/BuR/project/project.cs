﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

using masscarade.product_definition;

namespace masscarade.BuR.project
{
    [DataContract]
    public class Project : generation.Project
    {   
        [DataMember]
        public Settings settings { get; private set; }

        public DirectoryInfo location { get; private set; }
        
        public physical.SwConfiguration swConfiguration { get; private set; }        

        public static Project getSample()
        {
            return new Project()
            {
                settings = Settings.getSample(),                
            };
        }

        private void copyConfiguration(Order order)
        {
            //copy config
            DirectoryInfo srcConfigDir = new DirectoryInfo(
                Directory.GetDirectories(location .FullName + @"/Physical/", settings.newConfig.sourceConfig, SearchOption.TopDirectoryOnly)[0]);

            string destPath = srcConfigDir.Parent.FullName + @"/" + order.id.ToString();

            if (Directory.Exists(destPath) == true)
            {
                Directory.Delete(destPath, true);
            }

            CloneDirectory.clone(srcConfigDir.FullName, destPath);

            /*add new config to pkg file*/
            /*add folder for the feature type if not existing*/
            string pkgDir = Directory.GetFiles(srcConfigDir.Parent.FullName, "*.pkg", SearchOption.TopDirectoryOnly)[0];
            package.Physical pkg = package.Physical.load(pkgDir);

            pkg.add(new package.PhysicalObjectsObject()
            {
                Type = "Configuration",
                Description = "Auto generated",
                Value = order.id.ToString()
            });

            pkg.save(pkgDir);

            foreach (Reference reference in settings.newConfig.references)
            {
                reference.copy(this, order.id.ToString());
            }
        }

        public override void generate(Order order, product_definition.ProductSource productSource)
        {
            location = settings.source.getProject();
            //copy configuration
            copyConfiguration(order);

            loadSwConfig(order.id.ToString());

            ProductDefinition.Factory productDefinitionFactory = new ProductDefinition.Factory(productSource);

            MetaObject.Factory factory = new MetaObject.Factory();

            MetaObjectContainer metaObjects = factory.produce(order, productDefinitionFactory.produced);

            foreach (KeyValuePair<product_instance.ProductInstance, List<MetaObject>> pair in metaObjects)
            {
                foreach (MetaObject obj in pair.Value)
                {
                    obj.generate(this);
                }
            }

            swConfiguration.save();

            throw new NotImplementedException();
        }

        private void loadSwConfig(string configName)
        {
            //use first found sw config
            string swConfigPath =  Directory.GetFiles(location + @"\Physical\" + configName, @"Cpu.sw", SearchOption.AllDirectories)[0];
            //get original sw config
            swConfiguration = physical.SwConfiguration.load(swConfigPath);
        }

        [DataContract]
        public class Settings
        {
            [DataMember]
            public ProjectSource source { get; private set; }

            [DataMember]
            public NewConfig newConfig { get; private set; }

            public static Settings getSample()
            {
                return new Settings()
                {
                    source = ProjectSource.Local.getSample(),
                    newConfig = NewConfig.getSample(),
                };
            }

            [DataContract]
            public class NewConfig
            {
                [DataMember]
                public string sourceConfig { get; private set; }
                [DataMember]
                public List<Reference> references { get; private set; }

                public static NewConfig getSample()
                {
                    return new NewConfig()
                    {
                        sourceConfig = "name_of_the_config_that_should_be_copied",
                        references = new List<Reference>(new Reference[] 
                        {                           
                            Reference.MappControl.getSample(),
                            Reference.MappServices.getSample(),
                        }),
                    };
                }

            }            
        }

        [DataContract]
        [KnownType(typeof(MappServices))]
        [KnownType(typeof(MappControl))]
        public abstract class Reference
        {
            [DataMember]
            public string sourceConfig { get; protected set; }

            public abstract string type { get; }          

            [DataContract]
            public class MappServices : Reference
            {
                public override string type
                {
                    get
                    {
                        return "mappServices";
                    }
                }

                public static MappServices getSample()
                {
                    return new MappServices()
                    {
                        sourceConfig = "name_of_configuration",
                    };
                }
            }          

            [DataContract]
            public class MappControl : Reference
            {
                public override string type
                {
                    get
                    {
                        return "mappControl";
                    }
                }

                public static MappControl getSample()
                {
                    return new MappControl()
                    {
                        sourceConfig = "name_of_configuration",
                    };
                }
            }

            public void copy(Project project, string newConfig)
            {
                string destPath =
                project.location.GetDirectories("Physical", SearchOption.TopDirectoryOnly)[0]
                    .GetDirectories(newConfig, SearchOption.TopDirectoryOnly)[0]
                    .GetDirectories(type, SearchOption.AllDirectories)[0].FullName;

                string srcPath =
                project.location.GetDirectories("Physical", SearchOption.TopDirectoryOnly)[0]
                   .GetDirectories(sourceConfig, SearchOption.TopDirectoryOnly)[0]
                   .GetDirectories(type, SearchOption.AllDirectories)[0].FullName;

                if (Directory.Exists(destPath) == true)
                {
                    Directory.Delete(destPath, true);
                }

                Directory.CreateDirectory(destPath);

                CloneDirectory.clone(srcPath, destPath);
            }

            protected string getDestConfigPath(Settings parent, string newName)
            {
                return parent.source + @"/Physical/" + newName;
            }
            protected string getSrcConfigPath(Settings parent)
            {
                return parent.source + @"/Physical/" + this.sourceConfig;
            }

            protected virtual string getSrcPath(Settings parent)
            {
                return Directory.GetDirectories(getSrcConfigPath(parent), type, SearchOption.AllDirectories)[0];
            }
            protected virtual string getDestPath(Settings parent, string newName)
            {
                string path = getDestConfigPath(parent, newName);
                return Directory.GetDirectories(path, type, SearchOption.AllDirectories)[0];
            }
        }
    }

    [DataContract]
    [KnownType(typeof(Local))]
    [KnownType(typeof(Git))]
    [KnownType(typeof(TFS))]
    public abstract class ProjectSource : IEquatable<ProjectSource>
    {
        public new abstract int GetHashCode();

        public void fetch(DirectoryInfo dest)
        {
        }

        [DataContract]
        public class Local : ProjectSource
        {
            [DataMember]
            public string path { get; private set; }

            public static Local getSample()
            {
                return new Local()
                {
                    path = @"sample/path/to/project/",
                };
            }

            public override bool Equals(ProjectSource other)
            {
                if (other.GetType() == this.GetType())
                {
                    Local obj = (Local)other;

                    return this.path == obj.path;
                }

                return false;
            }

            public override int GetHashCode()
            {
                return path.GetHashCode();
            }

            public override DirectoryInfo getProject()
            {
                return new DirectoryInfo(path);
            }
        }

        [DataContract]
        public class Git : ProjectSource
        {
            public override bool Equals(ProjectSource other)
            {
                throw new NotImplementedException();
            }

            public static Git getSample()
            {
                return new Git();
            }

            public override int GetHashCode()
            {
                throw new NotImplementedException();
            }

            public override DirectoryInfo getProject()
            {
                throw new NotImplementedException();
            }
        }
        [DataContract]
        public class TFS : ProjectSource
        {
            public override bool Equals(ProjectSource other)
            {
                throw new NotImplementedException();
            }

            public static TFS getSample()
            {
                return new TFS();
            }

            public override int GetHashCode()
            {
                throw new NotImplementedException();
            }

            public override DirectoryInfo getProject()
            {
                throw new NotImplementedException();
            }
        }

        public abstract bool Equals(ProjectSource other);
        public abstract DirectoryInfo getProject();
    }   
}
