﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace masscarade.BuR.package
{
    public partial class PackageObject : IComparable<PackageObject>
    {
        public int CompareTo(PackageObject other)
        {
            return this.Value.CompareTo(other.Value) | this.typeField.CompareTo(other.Type);
        }      
    }


    public partial class Package
    {
        [XmlAttribute("PackageType")]
        public string packageType;

        public static Package load(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Package));
                return (Package)serializer.Deserialize(fs);
            }
        }
        public void save(string path)
        {         
            using (TextWriter writer = new StreamWriter(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Package));
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                serializer.Serialize(writer, this);
            }
        }
        public void add(PackageObject inst)
        {
            List<PackageObject> objects = new List<PackageObject>();

            if (this.objectsField != null)
            {
                objects.AddRange(this.objectsField);

                foreach (PackageObject obj in objects)
                {
                    if (obj.CompareTo(inst) == 0)
                    {
                        return;
                    }
                }
            }
          
            objects.Add(inst);

            this.objectsField = objects.ToArray();
        }
    }

    /* public partial class PackageObject : IComparable<PackageObject>
     {
         public int CompareTo(PackageObject other)
         {
             return this.Value.CompareTo(other.Value) | this.typeField.CompareTo(other.Type);
         }
     }*/

    public partial class PhysicalObjectsObject : IEquatable<PhysicalObjectsObject>
    {
        public bool Equals(PhysicalObjectsObject other)
        {
            if (this.Value == other.Value)
            {
                return true;
            }

            return false;
        }
    }

    public partial class Physical
    {
        public static Physical load(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Physical));
                return (Physical)serializer.Deserialize(fs);
            }
        }
        public void save(string path)
        {
            using (TextWriter writer = new StreamWriter(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Physical));
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                serializer.Serialize(writer, this);
            }
        }
        public void add(PhysicalObjectsObject inst)
        {
            List<PhysicalObjectsObject> objects = new List<PhysicalObjectsObject>(this.Items[0].Object);


            if(objects.Contains(inst) == false)
            {
                objects.Add(inst);
                this.Items[0].Object = objects.ToArray();
            }      
        }
    }
}