﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using masscarade.product_instance;
using masscarade.BuR.project;

namespace masscarade.BuR.logical
{
    public class Task : MetaObject
    {
        public const string EXTENSION = ".prg";     

        public static MetaObject produce(ProductInstance inst, ConfigurationObject obj, FileInfo src)
        {
            return new Task()
            {
                obj = obj,
                sourceData = src,
                instance = inst,
            };
        }      

        public override void generate(Project project)
        {
            project.swConfiguration.add(this);
        }
    }

    [DataContract]
    class TaskProperties : ConfigurationObject.BuRProperties
    {
        [DataMember]
        public TaskClass taskClass { get; protected set; }

        [DataMember]
        public bool debugging { get; protected set; }

        [DataMember]
        public Memory memory { get; protected set; }

        [DataMember]
        public Language language { get; protected set; }

        public static TaskProperties getSample()
        {
            return new TaskProperties()
            {
                taskClass = new TaskClass.Cyclic1(),
                debugging = true,
                memory = new Memory.UserRom(),
                language = new Language.ANSIC(),
                identPattern = new Regex("is_replaced_by_feature_instance_name", RegexOptions.None),
            };
        }

        [DataContract]
        [KnownType(typeof(UserRom))]
        public abstract class Memory
        {
            public abstract string text { get; }

            [DataContract]
            public class UserRom : Memory
            {
                public override string text
                {
                    get
                    {
                        return "UserROM";
                    }
                }
            }
        }

        [DataContract]
        [KnownType(typeof(ANSIC))]
        public abstract class Language
        {
            public abstract string text { get; }

            [DataContract]
            public class ANSIC : Language
            {
                public override string text
                {
                    get
                    {
                        return "ANSIC";
                    }
                }
            }
        }

        [DataContract]
        [KnownType(typeof(Cyclic1))]
        [KnownType(typeof(Cyclic2))]
        [KnownType(typeof(Cyclic3))]
        [KnownType(typeof(Cyclic4))]
        [KnownType(typeof(Cyclic5))]
        [KnownType(typeof(Cyclic6))]
        [KnownType(typeof(Cyclic7))]
        [KnownType(typeof(Cyclic8))]
        public abstract class TaskClass
        {
            public abstract string text { get; }

            [DataContract]
            public class Cyclic1 : TaskClass
            {
                public override string text
                {
                    get
                    {
                        return "Cyclic#1";
                    }
                }
            }

            [DataContract]
            public class Cyclic2 : TaskClass
            {
                public override string text
                {
                    get
                    {
                        return "Cyclic#2";
                    }
                }
            }
            [DataContract]
            public class Cyclic3 : TaskClass
            {
                public override string text
                {
                    get
                    {
                        return "Cyclic#3";
                    }
                }
            }
            [DataContract]
            public class Cyclic4 : TaskClass
            {
                public override string text
                {
                    get
                    {
                        return "Cyclic#4";
                    }
                }
            }

            [DataContract]
            public class Cyclic5 : TaskClass
            {
                public override string text
                {
                    get
                    {
                        return "Cyclic#5";
                    }
                }
            }

            [DataContract]
            public class Cyclic6 : TaskClass
            {
                public override string text
                {
                    get
                    {
                        return "Cyclic#6";
                    }
                }
            }

            [DataContract]
            public class Cyclic7 : TaskClass
            {
                public override string text
                {
                    get
                    {
                        return "Cyclic#7";
                    }
                }
            }
            [DataContract]
            public class Cyclic8 : TaskClass
            {
                public override string text
                {
                    get
                    {
                        return "Cyclic#8";
                    }
                }
            }
        }
    }
}
