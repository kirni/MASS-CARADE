﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

using masscarade.product_definition.feature.rules;
using masscarade.product_definition.feature;
using masscarade.product_definition;

namespace masscarade.product_instance
{
    [DataContract]
    public class ProductInstance : IEquatable<ProductInstance>
    {
        public Factory producer { get; private set; }

        /// <summary>
        /// is used to identify the product instance
        /// </summary>
        [DataMember]
        public ID id { get; private set; }

        [DataMember]
        public PropertyCollection properties { get; private set; }

        [DataMember]
        public List<SlotElement> slots { get; private set; }

        [DataMember]
        public ProductDefinition definition { get; set; }

        public override string ToString()
        {
            return id.ToString();
        }

        private ProductInstance()
        {

        }

        public bool Equals(ProductInstance other)
        {
            return id.Equals(other.id);
        }

        public class Factory
        {
            public Dictionary<ProductDefinition.ID, int> producedProducts { get; private set; } = new Dictionary<ProductDefinition.ID, int>();
            public Dictionary<Feature.Type, int> producedTypes { get; private set; } = new Dictionary<Feature.Type, int>();

            public ProductInstanceDictionary produced { get; private set; } = new ProductInstanceDictionary();

            private ID gernerateID(ProductDefinition definition)
            {
                return new ID
                    (
                        String.Format("{0}{1}", definition.feature.type, producedTypes[definition.feature.type]),
                        definition.id
                    );
            }
            public ProductInstance produce(ProductDefinition definition)
            {

                incrementDictionary<ProductDefinition.ID>(producedProducts, definition.id);
                incrementDictionary<Feature.Type>(producedTypes, definition.feature.type);

                ProductInstance inst = new ProductInstance()
                {
                    producer = this,
                    id = gernerateID(definition),
                    definition = definition,
                    properties = definition.properties.deepClone(),
                    slots = new List<SlotElement>(),
                };

                if (definition.rules != null)
                {
                    if (definition.rules.slots != null)
                    {
                        foreach (SlotDefinition def in definition.rules.slots)
                        {
                            inst.slots.Add(new SlotElement(def));
                        }
                    }
                }

                produced.Add(inst);

                return inst;
            }

            private static void incrementDictionary<T>(Dictionary<T, int> dictionary, T element)
            {
                if (dictionary.ContainsKey(element) == false)
                {
                    dictionary.Add(element, 0);
                }

                dictionary[element]++;
            }
        }

        [DataContract]
        public class ID : IEquatable<ID>
        {
            [DataMember]
            public string name { get; private set; }
            public ProductDefinition.ID productSource { get; private set; }

            public ID(string name, ProductDefinition.ID productSource)
            {
                this.name = name;
                this.productSource = productSource;
            }

            public override string ToString()
            {
                return name;
            }

            public override int GetHashCode()
            {
                return name.GetHashCode();
            }

            public bool Equals(ID other)
            {
                return this.name.Equals(other.name);
            }
        }
    }
    [CollectionDataContract]
    public class ProductInstanceDictionary : Dictionary<ProductInstance.ID, ProductInstance>
    {
        public void Add(ProductInstance value)
        {
            this.Add(value.id, value);
        }

        public void Add(ProductInstanceDictionary value)
        {
            foreach (KeyValuePair<ProductInstance.ID, ProductInstance> pair in this)
            {
                this.Add(pair.Value);
            }
        }

        public static ProductInstanceDictionary getSample()
        {
            ProductInstanceDictionary inst = new ProductInstanceDictionary();

            ProductInstance.Factory factory = new ProductInstance.Factory();

            ProductInstance obj = factory.produce(ProductDefinition.Factory.getSample());
            obj.slots.Add(new SlotElement(SlotDefinition.getSample()));
            obj.slots[0].Add(factory.produce(ProductDefinition.Factory.getSample()));            
            inst.Add(obj);

            inst.Add(factory.produce(ProductDefinition.Factory.getSample()));

            return inst;
        }

        /// <summary>
        /// get a collection containing all product instances with a one or more tags of the collection
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public ProductInstanceDictionary this[ICollection<Tag> key]
        {
            get
            {
                ProductInstanceDictionary items = new ProductInstanceDictionary();

                foreach (Tag tag in key)
                {
                    items.Add(this[tag]);
                }

                return items;
            }
        }

        /// <summary>
        /// get a collection containing all product instances with a tag
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public ProductInstanceDictionary this[Tag key]
        {
            get
            {
                ProductInstanceDictionary items = new ProductInstanceDictionary();

                foreach (KeyValuePair<ProductInstance.ID, ProductInstance> pair in this)
                {
                    if (pair.Value.definition.feature.tags.Contains(key))
                    {
                        items.Add(pair.Value);
                    }
                }

                return items;
            }
        }
    }
}
