﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.ComponentModel;

using masscarade.product_definition;
using masscarade.product_instance;

namespace masscarade
{
    /// <summary>
    /// defines a machine. consists of product instances with filled in properties
    /// </summary>
    [DataContract]
    public class Order : INotifyPropertyChanged, IEquatable<Order>
    {
        private ID _id = new ID("testID");

        /// <summary>
        /// identifies the order
        /// </summary>
        [DataMember]
        public ID id
        {
            get
            {
                return _id;
            }            
            set
            {
                _id = value;
                onPropertyChanged(new PropertyChangedEventArgs("id"));
            }
        }

        private DateTime _orderDate;

        /// <summary>
        /// gets updated when the order is changed
        /// </summary>
        [DataMember]
        public DateTime orderDate
        {
            get
            {
                return _orderDate;
            }

            set
            {
                _orderDate = value;
                onPropertyChanged(new PropertyChangedEventArgs("orderDate"));
            }
        }

        /// <summary>
        /// contains the configured product instances
        /// </summary>
        [DataMember]
        public ProductInstanceDictionary products { get; private set; } = new ProductInstanceDictionary();

        /// <summary>
        /// issued when a public property has been changed
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// creates a sample instance of an order
        /// </summary>
        /// <returns></returns>
        public static Order getSample()
        {
            Order inst = new Order()
            {
                products = ProductInstanceDictionary.getSample(),
                id = ID.getSample(),
                orderDate = new DateTime(),
            };

            return inst;
        }

        /// <summary>
        /// raises the Property changed event
        /// </summary>
        /// <param name="e"></param>
        public void onPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }

        /// <summary>
        /// orders are equal, when their IDs are equal
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Order other)
        {
            return id.Equals(other.id);
        }

        /// <summary>
        /// identifies the order
        /// </summary>
        [DataContract]
        public class ID : IEquatable<ID>
        {
            /// <summary>
            /// name of a sample tag
            /// </summary>
            public const string SAMPLE_NAME = "sample_id";

            /// <summary>
            /// count of generated samples. used internally for creating the names of samples
            /// </summary>
            public static int samplesGenerated { get; private set; } = 0;

            [DataMember]
            public string name { get; private set; }

            public ID(string name)
            {
                this.name = name;
            }

            /// <summary>
            /// returns the name
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return name;
            }

            /// <summary>
            /// generating a unique sample instance
            /// </summary>
            /// <returns></returns>
            public static ID getSample()
            {
                return new ID(String.Format("{0}{1}", SAMPLE_NAME, ++samplesGenerated))
                {
                };
            }

            /// <summary>
            /// order IDs are equal when their names are equal. Case sensitive!
            /// </summary>
            /// <param name="other"></param>
            /// <returns></returns>
            public bool Equals(ID other)
            {
                return name.Equals(other.name);
            }
        }
    }
}
