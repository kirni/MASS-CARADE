﻿
namespace masscarade
{
    /// <summary>
    /// Deep cloning means creating new instances rather than copying references
    /// </summary>
    /// <typeparam name="T">Type which is supposed to be cloned</typeparam>
    public interface IDeepCloneable<T>
    {
        /// <summary>
        /// creates a deep copy of this
        /// </summary>
        /// <returns>deep copy of this</returns>
        T deepClone();
    }
}
