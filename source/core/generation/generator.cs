﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace masscarade.generation
{
    [DataContract]
    public class Generator
    {
        public Project project { get; private set; }

        public Order order { get; private set; }

        public Settings settings { get; private set; }

        public Generator(Settings generatorSettings)
        {
            settings = generatorSettings;

            using (XmlReader reader = XmlReader.Create(settings.orderLocation))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(Order));

                order = (Order)serializer.ReadObject(reader);
            }

            using (XmlReader reader = XmlReader.Create(settings.projectSettingsLocation))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(BuR.project.Project));

                project = (BuR.project.Project)serializer.ReadObject(reader);
            }
        }

        public void generate()
        {
            project.generate(order, settings.productsLocation);
        }

        [DataContract]
        public class Settings
        {
            [DataMember]
            public string projectSettingsLocation { get; private set; }
            [DataMember]
            public string orderLocation { get; private set; }
            [DataMember]
            public product_definition.ProductSource productsLocation { get; private set; }

            public static Settings getSample()
            {
                return new Settings()
                {
                    orderLocation = "path_where_the_order_is",
                    projectSettingsLocation = "path_where_the_project_settings_are",
                    productsLocation = product_definition.ProductSource.Local.getSample(), 
                };
            }
        }
    }

    [DataContract]
    [KnownType(typeof(BuR.project.Project))]
    public abstract class Project
    {
        public abstract void generate(Order order, product_definition.ProductSource productSpurce);
    }
}
