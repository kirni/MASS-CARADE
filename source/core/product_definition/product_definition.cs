﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

using masscarade.product_definition.feature.rules;
using masscarade.product_definition.feature;

namespace masscarade.product_definition
{

    [DataContract]
    [KnownType(typeof(Local))]
    [KnownType(typeof(Git))]
    [KnownType(typeof(TFS))]
    public abstract class ProductSource : IEquatable<ProductSource>
    {
        public new abstract int GetHashCode();
     
        [DataContract]
        public class Local : ProductSource
        {
            [DataMember]
            public string path { get; private set; }

            public static Local getSample()
            {
                return new Local()
                {
                    path = @"sample/path/to/products/",
                };
            }

            public override bool Equals(ProductSource other)
            {
                if (other.GetType() == this.GetType())
                {
                    Local obj = (Local)other;

                    return this.path == obj.path;
                }

                return false;
            }

            public override int GetHashCode()
            {
                return path.GetHashCode();
            }

            public override DirectoryInfo getProducts()
            {
                return new DirectoryInfo(path);
            }
        }

        [DataContract]
        public class Git : ProductSource
        {
            public override bool Equals(ProductSource other)
            {
                throw new NotImplementedException();
            }

            public static Git getSample()
            {
                return new Git();
            }

            public override int GetHashCode()
            {
                throw new NotImplementedException();
            }

            public override DirectoryInfo getProducts()
            {
                throw new NotImplementedException();
            }
        }
        [DataContract]
        public class TFS : ProductSource
        {
            public override bool Equals(ProductSource other)
            {
                throw new NotImplementedException();
            }

            public static TFS getSample()
            {
                return new TFS();
            }

            public override int GetHashCode()
            {
                throw new NotImplementedException();
            }

            public override DirectoryInfo getProducts()
            {
                throw new NotImplementedException();
            }
        }

        public abstract bool Equals(ProductSource other);
        public abstract DirectoryInfo getProducts();
    }

    [DataContract]
    public class ProductDefinition : IEquatable<ProductDefinition>
    {
        /// <summary>
        /// is used to identify the product definition instance
        /// </summary>
        [DataMember]
        public ID id { get; private set; }

        public Feature feature { get; private set; }

        public PropertyCollection properties { get; private set; }

        public Rules rules { get; private set; }

        public Factory producer { get; private set; }

        private ProductDefinition()
        {
        }

        public override string ToString()
        {
            return String.Format("ID {0}", id);
        }

        public bool Equals(ProductDefinition other)
        {
            //the id is the unique idintiefier for a product
            return this.id.Equals(other.id);
        }

        /// <summary>
        /// returns true if the directory is a valid product definition.
        /// a directory is considered a product definition, when it contains a valid Feature.xml
        /// currently it is only checked, if the file is existing, not if it is valid
        /// </summary>
        /// <param name="dir">directory that should be checked</param>
        /// <returns></returns>
        public static bool isProductDefinition(DirectoryInfo dir)
        {
            return dir.GetFiles("Feature.xml", SearchOption.TopDirectoryOnly).Length == 1 || dir.GetFiles("feature.xml", SearchOption.TopDirectoryOnly).Length == 1;
        }

        public class Factory
        {
            /// <summary>
            /// contains all produced products
            /// </summary>
            public ProductDefinitionDictionary produced { get; private set; } = new ProductDefinitionDictionary();

            private HashSet<string> producedNames = new HashSet<string>();

            /// <summary>
            /// counter for produced samples; used to generate samle IDs
            /// </summary>
            private static int cSamples = 0;

            public ProductSource source { get; private set; }

            public ProductDefinition this[ID id]
            {
                get
                {
                    return produced[id];
                }
            }

            public Factory(ProductSource source)
            {
                this.source = source;

                DirectoryInfo products = source.getProducts();

                foreach (DirectoryInfo dir in products.GetDirectories("*", SearchOption.AllDirectories))
                {
                    if (ProductDefinition.isProductDefinition(dir))
                    {
                        produce(new ID(dir.Name, dir));
                    }
                }
            }

            /// <summary>
            /// creates a sample instance of Product, containing sample values for all properties
            /// </summary>
            /// <returns></returns>
            public static ProductDefinition getSample()
            {
                ProductDefinition inst = new ProductDefinition()
                {
                    id = new ID(String.Format("SampleProduct{0}", ++cSamples), new DirectoryInfo(String.Format("sample_path_{0}", cSamples))),
                    feature = Feature.getSample(),
                    properties = PropertyCollection.getSample(),
                };

                return inst;
            }

            public ProductDefinition produce(ID item)
            {

                if (produced.ContainsKey(item) == true)
                {
                    return produced[item];
                }

                ProductDefinition inst = new ProductDefinition()
                {
                    id = item,
                    producer = this,
                };

                //read properties if existing
                FileInfo[] propertiesPath = item.productSource.GetFiles("Properties.xml", SearchOption.TopDirectoryOnly);

                if (propertiesPath.Length == 1)
                {
                    if (propertiesPath[0].Exists)
                    {
                        using (XmlReader reader = XmlReader.Create(propertiesPath[0].FullName))
                        {
                            DataContractSerializer serializer = new DataContractSerializer(typeof(PropertyCollection));
                            inst.properties = (PropertyCollection)serializer.ReadObject(reader);
                        }
                    }
                }

                //read rules if existing
                FileInfo[] rulesPath = item.productSource.GetFiles("Rules.xml", SearchOption.TopDirectoryOnly);

                if (rulesPath.Length == 1)
                {
                    if (rulesPath[0].Exists)
                    {
                        using (XmlReader reader = XmlReader.Create(rulesPath[0].FullName))
                        {
                            DataContractSerializer serializer = new DataContractSerializer(typeof(Rules));
                            inst.rules = (Rules)serializer.ReadObject(reader);
                        }
                    }
                }

                //read feature; feature mustexist
                FileInfo[] featurePath = item.productSource.GetFiles("Feature.xml", SearchOption.TopDirectoryOnly);

                if (featurePath.Length == 1)
                {
                    if (featurePath[0].Exists)
                    {
                        using (XmlReader reader = XmlReader.Create(featurePath[0].FullName))
                        {
                            DataContractSerializer serializer = new DataContractSerializer(typeof(Feature));
                            inst.feature = (Feature)serializer.ReadObject(reader);
                        }
                    }
                }
                else
                {
                    throw new FeatureInvalidException(item);
                }

                //products must be unique
                produced.Add(inst);

                return inst;
            }

            public class FeatureInvalidException : Exception
            {
                private ID id;

                public FeatureInvalidException(ID id) : base()
                {
                    this.id = id;
                }

                public override string Message
                {
                    get
                    {
                        return String.Format("Product definition with ID <<{0}>> contains no or multible Feature.xml definitions", id);
                    }
                }
            }
        }

        [DataContract]
        public class ID : IEquatable<ID>
        {
            [DataMember]
            public string name { get; private set; }
            public DirectoryInfo productSource { get; private set; }

            public ID(string name, DirectoryInfo productSource)
            {
                this.name = name;
                this.productSource = productSource;
            }

            public override string ToString()
            {
                return name;
            }

            public override int GetHashCode()
            {
                return name.GetHashCode();
            }

            public bool Equals(ID other)
            {
                return this.name.Equals(other.name);
            }
        }
    }
    public class ProductDefinitionDictionary : Dictionary<ProductDefinition.ID, ProductDefinition>
    {
        public void Add(ProductDefinition value)
        {
            this.Add(value.id, value);
        }

        public void Add(ProductDefinitionDictionary value)
        {
            foreach (KeyValuePair<ProductDefinition.ID, ProductDefinition> pair in this)
            {
                this.Add(pair.Value);
            }
        }

        public static ProductDefinitionDictionary getSample()
        {
            ProductDefinitionDictionary sample = new ProductDefinitionDictionary();
            sample.Add(ProductDefinition.Factory.getSample());
            sample.Add(ProductDefinition.Factory.getSample());

            return sample;
        }

        public ProductDefinitionDictionary this[ICollection<Tag> key]
        {
            get
            {
                ProductDefinitionDictionary items = new ProductDefinitionDictionary();

                foreach (Tag tag in key)
                {
                    items.Add(this[tag]);
                }

                return items;
            }
        }

        public ProductDefinitionDictionary this[Condition condition]
        {
            get
            {
                ProductDefinitionDictionary items = new ProductDefinitionDictionary();

                foreach (KeyValuePair<ProductDefinition.ID, ProductDefinition> pair in this)
                {
                    if (condition.fulfillsCondition(pair.Value))
                    {
                        items.Add(pair.Value);
                    }
                }

                return items;
            }
        }

        public ProductDefinitionDictionary this[ICollection<Condition> conditions]
        {
            get
            {
                ProductDefinitionDictionary items = this;

                foreach (Condition condition in conditions)
                {
                    items = items[condition];
                }

                return items;
            }
        }

        public ProductDefinitionDictionary this[Tag key]
        {
            get
            {
                ProductDefinitionDictionary items = new ProductDefinitionDictionary();

                foreach (KeyValuePair<ProductDefinition.ID, ProductDefinition> pair in this)
                {
                    if (pair.Value.feature.tags.Contains(key))
                    {
                        items.Add(pair.Value);
                    }
                }

                return items;
            }
        }
    }   
}
