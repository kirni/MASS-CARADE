﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;
using System.Windows;
using masscarade.product_instance;

namespace masscarade.product_definition.feature.rules
{
    /// <summary>
    /// defines which and how many sub-products can be configured
    /// </summary>
    public class Rules
    {
        [DataMember]
        public List<SlotDefinition> slots { get; private set; } = new List<SlotDefinition>();
        public static Rules getSample()
        {
            Rules inst = new Rules();
            inst.slots.Add(SlotDefinition.getSample());

            return inst;
        }
    }

    /// <summary>
    /// a container for ProductInstances containing some rules for adding, defined by a SlotDefinition
    /// </summary>
    public class SlotElement : List<ProductInstance>
    {
        public SlotDefinition definition { get; private set; }

        /// <summary>
        /// true when the given <see cref="ProductDefinition"/> is allowed as child element for this slot
        /// </summary>
        /// <param name="productDefinition"> <see cref="ProductDefinition"/> which has to be checked</param>
        /// <returns>true when <see cref="ProductDefinition"/> is allowed as child element for this slot</returns>
        public bool productDefinitionValid(ProductDefinition productDefinition)
        {
            return definition.productDefinitionValid(productDefinition);
        }

        public SlotElement(SlotDefinition definition)
        {
            this.definition = definition;
        }

        private SlotElement()
        {
        }

        /// <summary>
        /// true if there is still more room for elements
        /// </summary>
        public bool addingAllowed
        {
            get
            {
                if (definition == null)
                {
                    return true;
                }
                return Count < definition.maxElements || definition.maxElements <= SlotDefinition.INFINITE_ELEMENTS;
            }
        }

        /// <summary>
        /// only adds if addingsAllowed is true, otherwise throws <see cref="SlotElementFullException"/>
        /// </summary>
        /// <param name="element"><see cref="ProductInstance"/> which should be added</param>
        /// <exception cref="SlotElementFullException"/>
        public new void Add(ProductInstance element)
        {
            if (addingAllowed == true)
            {
                base.Add(element);
            }
            else
            {
                throw new SlotElementFullException(element);
            }
        }

        /// <summary>
        /// thrown when a SlotElement is already full, but an add is attempted
        /// </summary>
        public class SlotElementFullException : Exception
        {
            /// <summary>
            /// instance which was attemped to be added
            /// </summary>
            private ProductInstance instance;

            /// <summary>
            /// requires Product instance which was attemped the be added
            /// </summary>
            /// <param name="instance"> Product instance which was attemped to be added</param>
            public SlotElementFullException(ProductInstance instance)
            {
                this.instance = instance;
            }

            public override string Message
            {
                get
                {
                    return base.Message + String.Format("Could not add {0}", instance);
                }
            }
        }
    }

    /// <summary>
    /// Defines how many and which products can be added
    /// </summary>
    [DataContract]
    public class SlotDefinition : IEquatable<SlotDefinition>
    {
        /// <summary>
        /// defines how many elements can be added to slot
        /// </summary>
        [DataMember]
        public int maxElements { get; private set; }
        /// <summary>
        /// conditions a ProductDefiniition has to fullfill fore being added to this slot
        /// </summary>
        [DataMember]
        public List<Condition> conditions { get; private set; } = new List<Condition>();
        /// <summary>
        /// when maxElements is set to this value, this slot has no limit for it's child elements
        /// </summary>
        public const int INFINITE_ELEMENTS = 0;

        /// <summary>
        /// returns true if <paramref name="productDefinition"/> can be added to this slot
        /// </summary>
        /// <param name="productDefinition">product definition which has to be checked</param>
        /// <returns>true if the product is valid for adding</returns>
        public bool productDefinitionValid(ProductDefinition productDefinition)
        {
            foreach (Condition condition in conditions)
            {
                if (condition.fulfillsCondition(productDefinition) == false)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// returns the object as a string
        /// </summary>
        /// <returns>a string with all conditions and the max elements</returns>
        /// <example>
        /// SlotDefinition def = SlotDefinition.getSample();
        /// Console.Writeline(def);
        /// </example>
        public override string ToString()
        {
            string str = "";

            foreach (Condition condition in conditions)
            {
                str += condition.ToString();
                str += " ";
            }

            if (maxElements > 0)
            {
                str += String.Format("Max {0} elements", maxElements);
            }

            return str;
        }

        public SlotDefinition(int maxElements = INFINITE_ELEMENTS)
        {
            this.maxElements = maxElements;
        }

        public SlotDefinition()
        {
            maxElements = INFINITE_ELEMENTS;
        }

        /// <summary>
        /// creates a sample instance of <see cref="SlotDefinition"/>
        /// </summary>
        public static SlotDefinition getSample()
        {
            SlotDefinition SlotDefinition = new SlotDefinition();

            //add all possible conditions
            SlotDefinition.conditions.Add(Condition.ContainsTag.getSample());
            SlotDefinition.conditions.Add(Condition.ExcludesTag.getSample());
            SlotDefinition.conditions.Add(Condition.TypeEqual.getSample());
            SlotDefinition.conditions.Add(Condition.TypeUnequal.getSample());

            return SlotDefinition;
        }

        /// <summary>
        /// a slot definition is considered equal if their conditions are equal
        /// </summary>
        /// <param name="other">instance which has to be compared to this</param>
        /// <returns>true if equal</returns>
        public bool Equals(SlotDefinition other)
        {
            return this.conditions.Equals(other.conditions);
        }
    }

    /// <summary>
    /// Base class for conditions applyable for <see cref="ProductDefinition"/> elements
    /// </summary>
    [DataContract]
    [KnownType(typeof(TypeEqual))]
    [KnownType(typeof(TypeUnequal))]
    [KnownType(typeof(ContainsTag))]
    [KnownType(typeof(ExcludesTag))]
    public abstract class Condition
    {
        /// <summary>
        /// checks if a <see cref="ProductDefinition"/> fulfills this condition
        /// </summary>
        /// <param name="inst"></param>
        /// <returns>true if this condition is fulfilled</returns>
        public abstract bool fulfillsCondition(ProductDefinition inst);

        /// <summary>
        /// checks if a product definition contains a tag
        /// </summary>
        [DataContract]
        public class ContainsTag : Condition
        {
            /// <summary>
            /// tag which has to be checked
            /// </summary>
            [DataMember]
            public Tag tag { get; private set; }

            public ContainsTag(Tag tag)
            {
                this.tag = tag;
            }

            /// <summary>
            /// creates a sample instance
            /// </summary>
            /// <returns>the sample</returns>
            public static ContainsTag getSample()
            {
                return new ContainsTag(Tag.getSample())
                {
                };
            }

            /// <summary>
            /// converts the condition to a string
            /// </summary>
            /// <returns>== "Tag"</returns>
            public override string ToString()
            {
                return String.Format("== {0}", tag.ToString());
            }

            /// <summary>
            /// checks if <paramref name="inst"/> contains <paramref name="tag"/>
            /// </summary>
            /// <param name="inst"></param>
            /// <returns>true if inst contains tag of this</returns>
            public override bool fulfillsCondition(ProductDefinition inst)
            {
                return inst.feature.tags.Contains(tag);
            }
        }

        /// <summary>
        /// checks if a <see cref="ProductDefinition"/> excludes a tag
        /// </summary>
        [DataContract]
        public class ExcludesTag : Condition
        {
            /// <summary>
            /// tag which has to be checked
            /// </summary>
            [DataMember]
            public Tag tag { get; private set; }

            public ExcludesTag(Tag tag)
            {
                this.tag = tag;
            }

            /// <summary>
            /// converts the condition to a string
            /// </summary>
            /// <returns>!= "Tag"</returns>
            public override string ToString()
            {
                return String.Format("!= {0}", tag.ToString());
            }

            /// <summary>
            /// creates a sample instance
            /// </summary>
            /// <returns>the sample</returns>
            public static ExcludesTag getSample()
            {
                return new ExcludesTag(Tag.getSample())
                {
                };
            }

            /// <summary>
            /// checks if <paramref name="inst"/> excludes <paramref name="tag"/>
            /// </summary>
            /// <param name="inst"></param>
            /// <returns>true if inst excludes tag of this</returns>
            public override bool fulfillsCondition(ProductDefinition inst)
            {
                return !inst.feature.tags.Contains(tag);
            }
        }

        /// <summary>
        /// checks if a <see cref="ProductDefinition"/> is of a specific <see cref="Type"/>
        /// </summary>
        [DataContract]
        public class TypeEqual : Condition
        {
            /// <summary>
            /// type which has to be checked
            /// </summary>
            [DataMember]
            public Feature.Type type { get; private set; }

            public TypeEqual(Feature.Type type)
            {
                this.type = type;
            }

            /// <summary>
            /// converts the condition to a string
            /// </summary>
            /// <returns>== "Type"</returns>
            public override string ToString()
            {
                return String.Format("== {0}", type.ToString());
            }

            /// <summary>
            /// checks if <paramref name="inst"/> is of the type <paramref name="type"/>
            /// </summary>
            /// <param name="inst"></param>
            /// <returns>true if inst is of the type of this
            public override bool fulfillsCondition(ProductDefinition inst)
            {
                return inst.feature.type.Equals(type);
            }

            /// <summary>
            /// creates a sample instance
            /// </summary>
            /// <returns>the sample</returns>
            public static TypeEqual getSample()
            {
                return new TypeEqual(Feature.Type.getSample())
                {
                };
            }
        }

        /// <summary>
        /// checks if a <see cref="ProductDefinition"/> is not of a specific <see cref="Type"/>
        /// </summary>
        [DataContract]
        public class TypeUnequal : Condition
        {
            /// <summary>
            /// type which has to be checked
            /// </summary>
            [DataMember]
            public Feature.Type type { get; private set; }

            public TypeUnequal(Feature.Type type)
            {
                this.type = type;
            }

            /// <summary>
            /// converts the condition to a string
            /// </summary>
            /// <returns>!= "Type"</returns>
            public override string ToString()
            {
                return String.Format("!= {0}", type.ToString());
            }

            /// <summary>
            /// checks if <paramref name="inst"/> is not of the type <paramref name="type"/>
            /// </summary>
            /// <param name="inst"></param>
            /// <returns>true if inst is not of the type of this
            public override bool fulfillsCondition(ProductDefinition inst)
            {
                return !inst.feature.type.Equals(type);
            }

            /// <summary>
            /// creates a sample instance
            /// </summary>
            /// <returns>the sample</returns>
            public static TypeUnequal getSample()
            {
                return new TypeUnequal(Feature.Type.getSample())
                {
                };
            }
        }
    }
}
