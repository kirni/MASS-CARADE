﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;

namespace masscarade.product_definition.feature
{    
    /// <summary>
    /// abstract base class for a object defining a aspect of feature.
    /// </summary>
    [KnownType(typeof(BuR.ConfigurationObject))]
    [DataContract]
    public abstract class ConfigurationObject : IEquatable<ConfigurationObject>
    {
        /// <summary>
        /// issues the configuration of the configuration object
        /// </summary>
        /// <param name="inst"></param>
        //public abstract void configure(product_instance.ProductInstance inst);
        public abstract bool Equals(ConfigurationObject other);      

        /// <summary>
        /// abstract method forces inherited objects to implement GetHashCode. Is neede, since preferrd container for the class is a hashset
        /// </summary>
        /// <returns></returns>
        public new abstract int GetHashCode();

        [DataMember]
        public PropetiesList properties { get; protected set; }

        [DataContract]
        [KnownType(typeof(RegexOptions))]
        public class ReplacePattern : Properties
        {
            [DataMember]
            public string replacement { get; private set; }

            [DataMember]
            public Regex pattern { get; private set; }

            public static ReplacePattern getSample()
            {
                return new ReplacePattern()
                {
                    pattern = new Regex("search_for_this_text", RegexOptions.None)
                    {
                    },
                    replacement = "insert_this_text",
                };
            }
        }

        [DataContract]
        [KnownType(typeof(BuR.ConfigurationObject.BuRProperties))]
        [KnownType(typeof(ReplacePattern))]
        public abstract class Properties
        {
            [DataMember]
            public Regex identPattern { get; protected set; }
        }

        public class PropetiesList : List<Properties>
        {
            public PropetiesList(IEnumerable<Properties> collection) : base(collection)
            {
            }

            public PropetiesList() : base()
            {
            }

            public PropetiesList this[Type key]
            {
                get
                {
                    PropetiesList container = new PropetiesList();

                    foreach (Properties obj in this)
                    {
                        if (obj.GetType() == key)
                        {
                            container.Add(obj);            
                        }
                    }
                    return container;
                }
            }

            public PropetiesList this[Type[] keys]
            {
                get
                {
                    PropetiesList container = new PropetiesList();

                    foreach (Properties obj in this)
                    {
                        foreach (Type key in keys)
                        {
                            if (obj.GetType() == key)
                            {
                                container.Add(obj);
                                break;
                            }
                        }       
                    }
                    return container;
                }
            }
        }
    }

    public class ConfigurationObjects : HashSet<ConfigurationObject>
    {
        public static ConfigurationObjects getSample()
        {
            ConfigurationObjects objects = new ConfigurationObjects();
            objects.Add(BuR.ConfigFile.getSample());
            objects.Add(BuR.ConfigFolder.getSample());
            Regex abc = new Regex("abc");

            return objects;
        }

        public ConfigurationObjects this[Type key]
        {
            get
            {
                ConfigurationObjects container = new ConfigurationObjects();

                foreach (ConfigurationObject obj in this)
                {
                    if (obj.GetType() == key)
                    {
                        container.Add(obj);
                    }
                }
                return container;
            }
        }
    }

    /// <summary>
    /// defines the content of a product. i.e. which components are needed for instaciating a product and generate it
    /// </summary>
    [DataContract]
    [KnownType(typeof(RegexOptions))]
    public class Feature
    {
        [DataMember]
        public Type type { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public ConfigurationObjects configurationObjects { get; private set; }
        /// <summary>
        /// used for filter, or search for products with tags
        /// </summary>
        [DataMember]
        public TagCollection tags { get; private set; }

        public static Feature getSample()
        {
            return new Feature()
            {
                type = Type.getSample(),
                tags = TagCollection.getSample(),
                configurationObjects = ConfigurationObjects.getSample(),                
            };
        }

        /// <summary>
        /// the type class is used to group products and identify them
        /// </summary>
        [DataContract]
        public class Type : IEquatable<Type>
        {
            /// <summary>
            /// name of the type. is serialized with datacontract
            /// </summary>
            [DataMember]
            public string name { get; private set; }

            /// <summary>
            /// name of a sample tag
            /// </summary>
            public const string SAMPLE_NAME = "sample_type";

            /// <summary>
            /// count of generated samples. used internally for creating the names of samples
            /// </summary>
            public static int samplesGenerated { get; private set; } = 0;

            public Type(string name)
            {
                this.name = name;
            }

            //returns the name of the type
            public override string ToString()
            {
                return name;
            }

            /// <summary>
            /// returns the hash code based on the name
            /// </summary>
            /// <returns></returns>
            public override int GetHashCode()
            {
                return name.GetHashCode();
            }

            /// <summary>
            /// types are equal when their names are equal. the case is ignored
            /// </summary>
            /// <param name="other"></param>
            /// <returns></returns>
            public bool Equals(Type other)
            {
                //convert strings to lower for ignoring the case
                return this.name.ToLower().Equals(other.name.ToLower());
            }

            public static Type getSample()
            {
                return new Type(String.Format("{0}{1}", SAMPLE_NAME, ++samplesGenerated))
                {
                };
            }
        }
    }

    /// <summary>
    /// Tag instances are used for tagging products.
    /// </summary>
    [DataContract]
    public class Tag : IEquatable<Tag>
    {
        /// <summary>
        /// name of the tag.
        /// </summary>
        [DataMember]
        public string name { get; private set; }

        /// <summary>
        /// name of a sample tag
        /// </summary>
        public const string SAMPLE_NAME = "sample_tag";

        /// <summary>
        /// count of generated samples. used internally for creating the names of samples
        /// </summary>
        public static int samplesGenerated { get; private set; } = 0;

        public Tag(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// tags are equal, when the names are equal. the case is ignored
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Tag other)
        {
            //ignoer case by converting all string to lower
            return this.name.ToLower().Equals(other.name.ToLower());
        }

        /// <summary>
        /// the hash code is calculated on the name
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return name.GetHashCode();
        }

        /// <summary>
        /// generates a sample Tag based on the SAMPLE_NAME und samplesGenerated
        /// <see cref="SAMPLE_NAME"/>
        /// <see cref="samplesGenerated"/>
        /// </summary>
        /// <returns></returns>
        public static Tag getSample()
        {
            return new Tag(String.Format("{0}{1}", SAMPLE_NAME, ++samplesGenerated))
            {
                //name = ,
            };
        }

        /// <summary>
        /// returns the name
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return name;
        }
    }

    /// <summary>
    /// preferred tag collection is a hashset, only unique tags in a collection make sense
    /// </summary>
    public class TagCollection : HashSet<Tag>
    {
        public static TagCollection getSample()
        {
            TagCollection collection = new TagCollection();
            collection.Add(Tag.getSample());
            collection.Add(Tag.getSample());
            collection.Add(Tag.getSample());

            return collection;
        }
    }
}

