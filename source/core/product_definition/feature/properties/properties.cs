﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;
using System.Windows;
using System.Collections.ObjectModel;


namespace masscarade.product_definition
{
    /// <summary>
    /// properties define which data of a <see cref="ProductDefinition"/> are supposed to be displayable as in or outputs
    /// </summary>
    [DataContract] 
    public abstract class Property : IEquatable<Property>, IDeepCloneable<Property>
    {
       // public abstract ObservableCollection<UIElement> uiElements { get; protected set; }

        /// <summary>
        /// describes the property and is need for every inherited object
        /// </summary>
        [DataMember]
        public abstract string description { get; protected set; }

        /// <summary>
        /// iherited by <see cref="IEquatable{T}"/> and implemented abstract
        /// </summary>
        /// <returns>returns true if other equals this</returns>
        public abstract bool Equals(Property other);

        /// <summary>
        /// generates a hahs code
        /// </summary>
        /// <returns>the hash code</returns>
        public abstract override int GetHashCode();

        public Property(string description)
        {
            this.description = description;
        }

        /// <summary>
        /// is supposed to be implemented abstract
        /// </summary>
        public void loadValue()
        {
            // "notImplemented!!";
        }

        /// <summary>
        /// iherited by <see cref="IDeepCloneable{T}"/> and implemented abstract
        /// </summary>
        /// <returns>a deep copy of the Property</returns>
        public abstract Property deepClone();

        /*public class FixDataOutput : Property, IOutput
        {

            [DataMember]
            public string source { get; private set; }

            public FixDataOutput(string description, string source) : base(description)
            {
                this.source = source;
                value = loadValue();

                uiElements = new ObservableCollection<UIElement>();

                Grid grid = new Grid()
                {
                    Name = "_grid",
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                };

                //first column containing static label and description text
                grid.ColumnDefinitions.Add(new ColumnDefinition()
                {
                    Width = GridLength.Auto,
                });

                //second column containing destination and value
                grid.ColumnDefinitions.Add(new ColumnDefinition()
                {
                    Width = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                Label label = new Label()
                {
                    Name = "_label",
                    Content = "Output of FixData",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                label.SetValue(Grid.RowProperty, 0);
                label.SetValue(Grid.ColumnProperty, 0);

                grid.Children.Add(label);

                Label box1 = new Label()
                {
                    Name = "_description",
                    HorizontalAlignment = HorizontalAlignment.Right,
                };

                box1.SetValue(Grid.RowProperty, 0);
                box1.SetValue(Grid.ColumnProperty, 1);

                Binding box1Binding = new Binding("description")
                {
                    Mode = BindingMode.OneWay,
                };
                box1.SetBinding(Label.ContentProperty, box1Binding);
                grid.Children.Add(box1);

                Label box2 = new Label()
                {
                    Name = "_destination",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                box2.SetValue(Grid.RowProperty, 1);
                box2.SetValue(Grid.ColumnProperty, 0);

                Binding box2Binding = new Binding("source")
                {
                    Mode = BindingMode.OneWay,
                };
                box2.SetBinding(Label.ContentProperty, box2Binding);
                grid.Children.Add(box2);

                Label box3 = new Label()
                {
                    Name = "_value",
                    HorizontalAlignment = HorizontalAlignment.Right,
                };

                box3.SetValue(Grid.RowProperty, 1);
                box3.SetValue(Grid.ColumnProperty, 1);

                Binding box3Binding = new Binding("value")
                {
                    Mode = BindingMode.OneWay,
                };
                box3.SetBinding(Label.ContentProperty, box3Binding);
                grid.Children.Add(box3);

                uiElements.Add(new GroupBox()
                {
                    DataContext = this,
                    Content = grid,
                    Header = "Fix data " + description,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                });
            }

            [DataMember]
            public override string description { get; protected set; }

            public override ObservableCollection<UIElement> uiElements { get; protected set; }

            [DataMember]
            public string value { get; private set; }

            public override bool Equals(Property other)
            {
                if (other.GetType() == this.GetType())
                {
                    if (this.source == ((FixDataOutput)other).source)
                    {
                        return true;
                    }
                }
                return false;
            }

            public override int GetHashCode()
            {
                return source.GetHashCode();
            }

            public static FixDataOutput getSample()
            {
                return new FixDataOutput("emptyDescription", "emptySource")
                {
                };
            }

            public override Property deepClone()
            {
                return new FixDataOutput(this.description, this.source);
            }
        }

        [DataContract]
        public class DynamicInterlock : Property, IInput
        {
            public DynamicInterlock(string description) : base(description)
            {
            }

            public override string description
            {
                get
                {
                    throw new NotImplementedException();
                }

                protected set
                {
                    throw new NotImplementedException();
                }
            }

            public override ObservableCollection<UIElement> uiElements
            {
                get
                {
                    throw new NotImplementedException();
                }

                protected set
                {
                    throw new NotImplementedException();
                }
            }

            public string value
            {
                get
                {
                    throw new NotImplementedException();
                }

                set
                {
                    throw new NotImplementedException();
                }
            }

            public override bool Equals(Property other)
            {
                throw new NotImplementedException();
            }

            public override int GetHashCode()
            {
                throw new NotImplementedException();
            }

            public static DynamicInterlock getSample()
            {
                return new DynamicInterlock("emptyDescription")
                {
                };
            }

            public void apply(ProductInstance parent)
            {
                throw new NotImplementedException();
            }

            public override Property deepClone()
            {
                throw new NotImplementedException();
            }
        }

        [DataContract]
        public class StaticInterlock : Property, IInput
        {
            public StaticInterlock(string description) : base(description)
            {
            }

            public override string description
            {
                get
                {
                    throw new NotImplementedException();
                }

                protected set
                {
                    throw new NotImplementedException();
                }
            }

            public override ObservableCollection<UIElement> uiElements
            {
                get
                {
                    throw new NotImplementedException();
                }

                protected set
                {
                    throw new NotImplementedException();
                }
            }

            public string value
            {
                get
                {
                    throw new NotImplementedException();
                }

                set
                {
                    throw new NotImplementedException();
                }
            }

            public override bool Equals(Property other)
            {
                throw new NotImplementedException();
            }

            public override int GetHashCode()
            {
                throw new NotImplementedException();
            }

            public static StaticInterlock getSample()
            {
                return new StaticInterlock("emptyDescription")
                {
                };
            }

            public void apply(ProductInstance parent)
            {
                throw new NotImplementedException();
            }

            public override Property deepClone()
            {
                throw new NotImplementedException();
            }
        }

        [DataContract]
        public class FixDataInput : Property, IInput
        {
            [DataMember]
            public string destination { get; private set; } = "emptyDestination";

            [DataMember]
            public string value { get; set; } = "emptyValue";

            public override ObservableCollection<UIElement> uiElements { get; protected set; }
            public override string description { get; protected set; } = "emptyDescription";

            public FixDataInput(string destination, string description) : base(description)
            {
                this.destination = destination;

                value = loadValue();

                uiElements = new ObservableCollection<UIElement>();

                Grid grid = new Grid()
                {
                    Name = "_grid",
                };

                //first column containing static label and description text
                grid.ColumnDefinitions.Add(new ColumnDefinition()
                {
                    Width = GridLength.Auto,
                });

                //second column containing destination and value
                grid.ColumnDefinitions.Add(new ColumnDefinition()
                {
                    Width = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                Label label = new Label()
                {
                    Name = "_label",
                    Content = "Input to FixData",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                label.SetValue(Grid.RowProperty, 0);
                label.SetValue(Grid.ColumnProperty, 0);

                grid.Children.Add(label);

                Label box1 = new Label()
                {
                    Name = "_description",
                    HorizontalAlignment = HorizontalAlignment.Right,
                };

                box1.SetValue(Grid.RowProperty, 0);
                box1.SetValue(Grid.ColumnProperty, 1);

                Binding box1Binding = new Binding("description")
                {
                    Mode = BindingMode.OneWay,
                };
                box1.SetBinding(Label.ContentProperty, box1Binding);
                grid.Children.Add(box1);

                Label box2 = new Label()
                {
                    Name = "_destination",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                box2.SetValue(Grid.RowProperty, 1);
                box2.SetValue(Grid.ColumnProperty, 0);

                Binding box2Binding = new Binding("destination")
                {
                    Mode = BindingMode.OneWay,
                };
                box2.SetBinding(Label.ContentProperty, box2Binding);
                grid.Children.Add(box2);

                TextBox box3 = new TextBox()
                {
                    Name = "_value",
                    IsReadOnly = false,
                    HorizontalAlignment = HorizontalAlignment.Right,
                };

                box3.SetValue(Grid.RowProperty, 1);
                box3.SetValue(Grid.ColumnProperty, 1);

                Binding box3Binding = new Binding("value")
                {
                    Mode = BindingMode.TwoWay,
                };
                box3.SetBinding(TextBox.TextProperty, box3Binding);
                grid.Children.Add(box3);

                uiElements.Add(new GroupBox()
                {
                    DataContext = this,
                    Content = grid,
                    Header = "Fix data " + description,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                });
            }

            public override bool Equals(Property other)
            {
                if (other.GetType() == this.GetType())
                {
                    if (this.destination == ((FixDataInput)other).destination)
                    {
                        return true;
                    }
                }
                return false;
            }

            public override int GetHashCode()
            {
                return destination.GetHashCode();
            }

            public static FixDataInput getSample()
            {
                return new FixDataInput("emptyDestination", "emptyDescription");
            }

            public void apply(ProductInstance parent)
            {
                throw new NotImplementedException();
            }

            public override Property deepClone()
            {
                return new FixDataInput(this.destination, this.description);
            }
        }*/

        /// <summary>
        /// defines a value property with public get
        /// </summary>
        public interface IOutput
        {
            string value
            {
                get;
            }
        }

        /// <summary>
        /// defines a value property with public get and set
        /// </summary>
        public interface IInput
        {
            string value
            {
                set;
                get;
            }

            //void apply(ProductInstance parent);
        }
    }

    /// <summary>
    /// collecion of properties. implemented as a hash set, since the entries should be unique
    /// </summary>
    //[DataContract]
    public class PropertyCollection : HashSet<Property>, IDeepCloneable<PropertyCollection>
    {
        /// <summary>
        /// generates a sample instance containing samles of all known properties
        /// </summary>
        /// <returns></returns>
        public static PropertyCollection getSample()
        {
            PropertyCollection collection = new PropertyCollection();
            //collection.Add(Property.FixDataInput.getSample());
            //collection.Add(Property.FixDataOutput.getSample());
            //collection.Add(Property.StaticInterlock.getSample());
            //collection.Add(Property.DynamicInterlock.getSample());

            return collection;
        }

        /// <summary>
        /// creates a deep copy of this
        /// </summary>
        /// <returns>deep copy of this</returns>
        public PropertyCollection deepClone()
        {
            PropertyCollection inst = new PropertyCollection();

            foreach (Property element in this)
            {
                inst.Add(element.deepClone());
            }

            return inst;
        }
    }
}
