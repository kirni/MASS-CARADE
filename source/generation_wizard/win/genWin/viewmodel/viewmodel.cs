﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using System.Runtime.Serialization;
using System.Xml;

using masscarade.generation;

namespace genWin.viewmodel
{
    class viewmodel
    {
        public viewmodel()
        {
            FileInfo settingsFile = new FileInfo("settings.xml");
            Generator.Settings settings;

            if (settingsFile.Exists == false)
            {
                settings = Generator.Settings.getSample();

                XmlWriterSettings xmlSettings = new XmlWriterSettings()
                {
                    Indent = true,
                };

                using (XmlWriter reader = XmlWriter.Create("settings.xml", xmlSettings))
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(Generator.Settings));

                    serializer.WriteObject(reader, settings);
                }
            }
            else
            {
                using (XmlReader reader = XmlReader.Create("settings.xml"))
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(Generator.Settings));

                    settings = (Generator.Settings)serializer.ReadObject(reader);
                }
            }

            Generator generator = new Generator(settings);
            generator.generate();
        }
    }
}
