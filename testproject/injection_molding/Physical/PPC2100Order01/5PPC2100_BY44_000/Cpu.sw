<?xml version="1.0" encoding="utf-8"?>
<SwConfiguration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" CpuAddress="SL1" xmlns="http://br-automation.co.at/AS/SwConfiguration">
  <TaskClass Name="Cyclic#1">
    <Task Name="Core1" Source="corepuller.Core.prg" Memory="UserROM" Language="ANSIC" Debugging="true" Description="auto generated CP001" />
    <Task Name="Core2" Source="corepuller.Core.prg" Memory="UserROM" Language="ANSIC" Debugging="true" Description="auto generated CP001" />
    <Task Name="Core3" Source="corepuller.Core.prg" Memory="UserROM" Language="ANSIC" Debugging="true" Description="auto generated CP001" />
    <Task Name="Core4" Source="corepuller.Core.prg" Memory="UserROM" Language="ANSIC" Debugging="true" Description="auto generated CP001" />
  </TaskClass>
  <TaskClass Name="Cyclic#2" />
  <TaskClass Name="Cyclic#3" />
  <TaskClass Name="Cyclic#4" />
  <Libraries>
    <LibraryObject Name="MpRecipe" Source="Libraries.MpRecipe.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MpBase" Source="Libraries.MpBase.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="sys_lib" Source="Libraries.sys_lib.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="runtime" Source="Libraries.runtime.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MpClamp" Source="Libraries.MpClamp.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MTBasics" Source="Libraries.MTBasics.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="brsystem" Source="Libraries.brsystem.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MTTypes" Source="Libraries.MTTypes.lby" Memory="UserROM" Language="Binary" Debugging="true" />
  </Libraries>
</SwConfiguration>